﻿using BocasayCodingTest.Models;
using System.Threading.Tasks;

namespace BocasayCodingTest.Services.Interfaces
{
    public interface IUserService
    {
        Task<User> Create(CreateUserRequest model);
    }
}
