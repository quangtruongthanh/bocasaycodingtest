﻿using BocasayCodingTest.Models;
using BocasayCodingTest.Repositories.Interfaces;
using BocasayCodingTest.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace BocasayCodingTest.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<User> Create(CreateUserRequest model)
        {
            var user = new User { FirstName = model.FirstName, LastName = model.LastName, Id = Guid.NewGuid().ToString() };
            return await Task.FromResult(_userRepository.AddAsync(user));
        }
    }
}
