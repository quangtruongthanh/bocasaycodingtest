﻿using BocasayCodingTest.Repositories.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace BocasayCodingTest.Repositories
{
    public class JsonFileStorageGenericRepository<T> : IGenericRepository<T>
    {
        private readonly string _jsonFile;
        private readonly object _locker = new object();
       public JsonFileStorageGenericRepository()
        {
            _jsonFile = Path.Combine(Directory.GetCurrentDirectory(), $"{typeof(T).Name.ToLower()}.json");
        }

        public T AddAsync(T data)
        {
            lock (_locker) {
                var objects = GetAll();
                objects.Add(data);
                SaveChanges(objects);
                return data;
            }
        }

        private void SaveChanges(List<T> objects)
        {
            using (var jsonStream = File.CreateText(_jsonFile))
            {
                Newtonsoft.Json.JsonSerializer jsonSerializer = new Newtonsoft.Json.JsonSerializer();
                jsonSerializer.Serialize(jsonStream, objects);
            }
        }

        private List<T> GetAll()
        {
            var objects = new List<T>();
            if (File.Exists(_jsonFile))
            {
                using (StreamReader jsonStream = File.OpenText(_jsonFile))
                {
                    objects = JsonSerializer.Deserialize<List<T>>(jsonStream.ReadToEnd());
                }
            }

            return objects;
        }
    }
}
