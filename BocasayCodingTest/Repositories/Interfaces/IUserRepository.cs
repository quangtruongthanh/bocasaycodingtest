﻿using BocasayCodingTest.Models;

namespace BocasayCodingTest.Repositories.Interfaces
{
    public interface IUserRepository: IGenericRepository<User> {}
}
