﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BocasayCodingTest.Repositories.Interfaces
{
    public interface IGenericRepository<T>
    {
        T AddAsync(T data);
    }
}
