﻿using BocasayCodingTest.Models;
using BocasayCodingTest.Repositories.Interfaces;

namespace BocasayCodingTest.Repositories
{
    public class JsonFileStorageUserRepository : JsonFileStorageGenericRepository<User>, IUserRepository  {}
}
