﻿using BocasayCodingTest.Models;
using BocasayCodingTest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BocasayCodingTest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DataController : ControllerBase
    {
        private readonly IUserService _userService;
        public DataController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<ObjectResult> CreateUser(CreateUserRequest model)
        {
            var result = await _userService.Create(model);
            return Ok(result);
        }
    }
}
