import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../common/model/user';
import { DataService } from '../common/service/data';


@Component({
  selector: 'create-user',
  templateUrl: './create-user.component.html',
})

export class CreateUserComponent {
  processing = false;
  errorMessage = "";
  formData = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
  });


  constructor(private fb: FormBuilder, private dataService: DataService) {
  }

  public onReset() {
    this.formData.patchValue({
      firstName: '',
      lastName: ''
    });
    this.errorMessage = "";
  }

  public onSubmit() {
    if (this.processing) return;
    this.processing = true;
    this.errorMessage = "";
    var data = this.formData.value;
    var user = new User();
    user.firstName = data.firstName;
    user.lastName = data.lastName;
    this.dataService.createUser(user).subscribe(result => {
      alert("Success!");
      this.formData.patchValue({
        firstName: '',
        lastName: ''
      });
    }, error => {
        console.error(error);
        if (error.message) {
          this.errorMessage = error.message;
        } else {
          this.errorMessage = "Failed!";
        }
    });

    this.processing = false;
  }
}
