import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { User } from '../model/user'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  createUser(userData: User) {
    var postUrl = this.baseUrl + 'api/data';
    return this.http.post<User>(postUrl, userData)
  }
}
