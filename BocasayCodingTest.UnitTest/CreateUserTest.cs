using BocasayCodingTest.Controllers;
using BocasayCodingTest.Models;
using BocasayCodingTest.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace BocasayCodingTest.UnitTest
{
    [TestClass]
    public class CreateUserTest
    {
        [TestMethod]
        public void CreateUserShouldSuccess()
        {
            var mock = new Mock<IUserService>();
            var requestModel = new CreateUserRequest { FirstName= "First Name", LastName = "Last Name"};
            var newUser = new User { FirstName = requestModel.FirstName, LastName = requestModel.LastName, Id = Guid.NewGuid().ToString()};

            mock.Setup(p => p.Create(requestModel)).Returns(Task.FromResult(newUser));
            
            DataController dataController = new DataController(mock.Object);
            var result = dataController.CreateUser(requestModel).Result;

            Assert.AreEqual((int)HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException), noExceptionMessage : "The FirstName field is required.")]
        public void CreateUserWithEmptyFirstNameShouldFail()
        {
            var requestModel = new CreateUserRequest { FirstName = "", LastName = "Last Name" };
            ValidateObject(requestModel);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException), noExceptionMessage : "The LastName field is required.")]
        public void CreateUserWithEmptyLastNameShouldFail()
        {
            var requestModel = new CreateUserRequest { FirstName = "First Name", LastName = "" };
            ValidateObject(requestModel);
        }

        private static void ValidateObject<T>(T obj)
        {
            var type = typeof(T);
            var meta = type.GetCustomAttributes(false).OfType<MetadataTypeAttribute>().FirstOrDefault();
            if (meta != null)
            {
                type = meta.MetadataClassType;
            }
            var propertyInfo = type.GetProperties();
            foreach (var info in propertyInfo)
            {
                var attributes = info.GetCustomAttributes(false).OfType<ValidationAttribute>();
                foreach (var attribute in attributes)
                {
                    var objPropInfo = obj.GetType().GetProperty(info.Name);
                    attribute.Validate(objPropInfo.GetValue(obj, null), info.Name);
                }
            }
        }
    }
}
